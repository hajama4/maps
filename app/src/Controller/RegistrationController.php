<?php

namespace App\Controller;

use App\Entity\User;
use App\Service\Registration\RegistrationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends AbstractController
{
    private $registrationService;

    public function __construct(RegistrationService $registrationService)
    {
        $this->registrationService = $registrationService;
    }

    /**
     * @Route("/register", name="user_registration")
     */
    public function registerAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $form = $this->registrationService->registration($request, $passwordEncoder);

        return ($form->isSubmitted() && $form->isValid())
                ? $this->redirectToRoute('home')
                : $this->render('registration/register.html.twig',
                    [
                        'form' => $form->createView(),
                    ]);
    }


}