<?php

namespace App\Controller;

use App\Service\Device\DeviceServiceInterface;
use App\Service\JsonResponse\FormJsonResponseServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DeviceController extends AbstractController
{
    private $deviceService;

    private $jsonResponseService;

    public function __construct(
        DeviceServiceInterface $deviceService,
        FormJsonResponseServiceInterface $jsonResponseService
    ) {
        $this->deviceService = $deviceService;
        $this->jsonResponseService = $jsonResponseService;
    }

    /**
     * @Route("/devices", name="devices")
     */
    public function index()
    {
        $devices = $this->deviceService->getUserDevices();

        return $this->jsonResponseService->respond($devices);
    }

    /**
     * @Route("/device/create", methods="POST")
     */
    public function create(Request $request)
    {

        if (!$request) {
            return $this->respondValidationError('Please provide a valid request!');
        }

        $data = json_decode($request->getContent($request), true);

        //TODO: create method to handle validation errors

        // validate device id
        if (!$data['deviceId']) {
            return $this->respondValidationError('Please provide a device id!');
        }

        // validate gps coordinates
        if (!$data['gps']) {
            return $this->respondValidationError('Please provide a gps coordinates!');
        }

        // validate type
        if (!$data['type']) {
            return $this->respondValidationError('Please provide coordinates type!');
        }

        $record = $this->deviceService->addNewDevice($data);

        //TODO Validate if no errors saving records in database

        $respond = $this->jsonResponseService;

        return $respond->respondCreated($record);
    }
}