<?php


namespace App\Service\JsonResponse;


use Symfony\Component\HttpFoundation\JsonResponse;

interface FormJsonResponseServiceInterface
{
    public function getStatusCode();

    public function setStatusCode($statusCode);

    public function respond($data, $headers = []);

    public function respondWithErrors($errors, $headers = []);

    public function respondUnauthorized($message = 'Not authorized!');

    public function respondValidationError($message = 'Validation errors');

    public function respondNotFound($message = 'Not found!');

    public function respondCreated($data = []);

}