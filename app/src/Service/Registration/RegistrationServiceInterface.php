<?php


namespace App\Service\Registration;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

interface RegistrationServiceInterface
{
    public function registration(Request $request, UserPasswordEncoderInterface $passwordEncoder);
}