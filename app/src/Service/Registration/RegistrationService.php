<?php


namespace App\Service\Registration;

use App\Domain\Registration\RegisterInterface;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationService implements RegistrationServiceInterface
{
    /**
     * @var App\Domain\Registration\Register
     */
    private $register;

    /**
     * RegistrationService constructor.
     * @param RegisterInterface $register
     */
    public function __construct(RegisterInterface $register)
    {
        $this->register = $register;
    }

    /**
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return mixed
     */
    public function registration(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = new User();
        $form = $this->register->createForm($request, $user);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $this->register->updateDatabase($user, $passwordEncoder);
        }

        return $form;
    }
}