<?php


namespace App\Service\Device;


use App\Domain\Device\DeviceInterface;

class DeviceService implements DeviceServiceInterface
{
    private $device;

    public function __construct(DeviceInterface $device)
    {
        $this->device = $device;
    }

    public function getUserDevices()
    {
        $deviceList = $this->device->getDeviceList();

        return $this->device->formatList($deviceList);
    }

    public function addNewDevice(array $device)
    {
        $record = $this->device->addRecord($device);

        return $this->device->formOne($record);//TODO: return error
    }
}