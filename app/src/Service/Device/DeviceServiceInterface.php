<?php


namespace App\Service\Device;


interface DeviceServiceInterface
{
    public function getUserDevices();

    public function addNewDevice(array $device);
}