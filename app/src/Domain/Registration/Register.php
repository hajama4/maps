<?php


namespace App\Domain\Registration;


use App\Entity\User;
use App\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class Register implements RegisterInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var App\Repository\UserRepository
     */
    private $userRepository;

    /**
     * @var Symfony\Component\Form\FormFactoryInterface
     */
    private $ff;
    /**
     * user active
     */
    private const ACTIVE = 1;

    /**
     * user deactivated
     */
    private const NOT_ACTIVE = 0;

    /**
     * admin level
     */
    private const ROLE_ADMIN = 2;

    /**
     * user level
     */
    private const ROLE_USER = 1;

    /**
     * Register constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, FormFactoryInterface $ff)
    {
        $this->em = $em;
        $this->userRepository = $em->getRepository("App:User");
        $this->ff = $ff;
    }

    /**
     * @param Request $request
     * @param User $user
     * @return \Symfony\Component\Form\FormInterface
     */
    public function createForm(Request $request, User $user)
    {
        $form = $this->ff->create(UserType::class, $user);
        $form->handleRequest($request);

        return $form;
    }

    /**
     * @param User $user
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function updateDatabase(User $user, UserPasswordEncoderInterface $passwordEncoder)
    {
        $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
        $user->setPassword($password);
        $user->setIsActive(self::ACTIVE);
        $user->setLevel(self::ROLE_USER);
        $this->em->persist($user);
        $this->em->flush();
    }
}