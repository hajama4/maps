<?php


namespace App\Domain\Registration;


use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

interface RegisterInterface
{
    public function createForm(Request $request, User $user);

    public function updateDatabase(User $user, UserPasswordEncoderInterface $passwordEncoder);
}