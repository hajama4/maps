<?php


namespace App\Domain\Device;


interface DeviceInterface
{
    public function getDeviceList();

    public function formatList(array $device);

    public function addRecord(array $record);

    public function formOne(\App\Entity\Device $device);

    public function deviceType($type);
}