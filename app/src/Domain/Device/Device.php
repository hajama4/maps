<?php


namespace App\Domain\Device;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;
use App\Entity\Device as DeviceEntity;

class Device implements DeviceInterface
{
    const TYPE_WORK = 1;

    const TYPE_HOME = 2;

    private $em;

    private $deviceRepository;

    private $security;

    public function __construct(EntityManagerInterface $em, Security $security)
    {
        $this->em = $em;
        $this->deviceRepository = $this->em->getRepository("App:Device");
        $this->security = $security;
    }

    public function getDeviceList()
    {
        return $this->deviceRepository->findByUser($this->security->getUser());
    }

    public function formatList(array $device)
    {
        $formattedList = [];

        foreach($device as $d) {
            $formattedList[] = [
                'deviceId' => $d->getDeviceId(),
                'gps' => $d->getGps(),
                'type' => $this->deviceType($d->getType()),
            ];
        }

        return $formattedList;
    }

    public function addRecord(array $record)
    {
        $device = new DeviceEntity();
        $device->setDeviceId($record['deviceId']);
        $device->setGps($record['gps']);
        $device->setType($record['type']);
        $device->setUser($this->security->getUser());
        $this->em->persist($device);
        $this->em->flush();

        return $device;
    }

    public function formOne(DeviceEntity $device)
    {
        $device = [
            'deviceId' => $device->getDeviceId(),
            'gps' => $device->getGps(),
            'type' => $this->deviceType($device->getType()),
        ];

        return $device;
    }

    public function deviceType($type)
    {
        switch ($type) {
            case self::TYPE_WORK:
                $type = 'Work';
                break;
            default:
                $type = 'Home';
                break;
        }

        return $type;
    }
}