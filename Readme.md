## Getting Started

### Clone the repository
```bash
git clone https://hajama4@bitbucket.org/hajama4/keliu-darbai.git
```

### Change directory
```bash
cd maps
```

### Create my sql folder named mysql
```bash
mkdir mysql
```

### Command to launch docker
```bash
docker-compose up -d --build
```

### Install the backend dependencies
### For windows
```bash
 winpty docker exec -it php-container bash
 composer install
```
### For others
```bash
 docker exec -it php-container bash
 composer install
```
### Add database connection to your database got to .env
```bash
DATABASE_URL="mysql://root:root@mysql_container:3306/Devicemap?serverVersion=5.7"
```

### To launch database migrations
```bash
docker-compose run --rm php-service php bin/console doctrine:migrations:migrate
```

### Install the frontend dependencies
```bash
$ docker-compose run --rm node-service yarn install
```

#### Compile file for Frontend 
```bash
docker-compose run --rm node-service yarn run encore dev --watch
```

### Open browser
```bash
url: localhost:8080
```